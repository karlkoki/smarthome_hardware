
<?php
$db = new SQLite3("../CCServer/ccserver.db");
$results = $db->query("SELECT hw.name AS hardwarename,hw.id,hw.IP,loc.name AS ort
                    FROM hardware AS hw
                    JOIN location AS loc
                    WHERE hw.location_id=loc.id
                    Order By loc.name");

$tbody = '<tbody id="devices_list_body">';

while ($row = $results->fetchArray()) {
    $tbody = $tbody . renderRow($row['id'], $row['hardwarename'], $row['IP'], $row['ort']);
}
$tbody = $tbody . "</tbody>";

function renderRow($id, $hw, $ip, $place) {
    return '
    <tr>
        <td>' . $id .'</td>
        <td> ' . $hw .' </td>
        <td> ' . $place . ' </td>
        <td>
            <section class="Dropdown">
                <button type="button" onclick="on_ButtonInfoClick(' . $id . ')" name="button_' . $id . '">Info</button>
                <article id="info_' . $id .'" class="Dropdown_hide">
           
                    <h5>Die IP-Adresse lautet: '. $ip .'</h5>
                    <button type="button" onclick="on_ButtonChangeClick(' . $id .')">Ändern</button> 
                </article>
            </section>
        </td>
    </tr>';
}
?>

<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="./style_index.css">

    <title>SmartHome Übersicht</title>
</head>
<body onload="onLoad()">
<header>
    <button class="Button_header" id="Button_Home" onclick="on_ButtonHomeClick();">Home</button>
    <section class="Dropdown">
        <button class="Button_header" id="Button_dropdown" onclick="on_ButtonMenueClick()">...</button>
        <article id="Dropdown_menue" class="Dropdown_hide">
            <a class="Dropdown_link" href="Kontakt.html">Kontakt</a>
            <a class="Dropdown_link" href="Hilfe.html">Hilfe</a>
        </article>
    </section>

    <h1>Übersicht über deine SmartHome Geräte:</h1>
</header>
<section id="sec_php">
</section>
<section id="devices">
    <table id="devices_list">
    <thead>
    <tr class="devices_listitem">
                <th>ID</th>
                <th>Bezeichnung</th>
                <th>Ort</th>
                <th>Infos</th>
            </tr>
</thead>
    <?php
        printf($tbody);
    ?>
    </table>
</section>
<section id="bottombuttons">
    <button class="Button_Bottom" id="Button_Aktualisieren" onclick="on_ButtonHomeClick()">Aktualisieren</button>
</section>
</body>

<script src="./script_index.js"></script>
</html>

