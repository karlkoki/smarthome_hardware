/*
 *  This sketch demonstrates how to set up a simple HTTP-like server.
 *  The server will set a GPIO pin depending on the request
 *    http://server_ip/gpio/0 will set the GPIO2 low,
 *    http://server_ip/gpio/1 will set the GPIO2 high
 *  server_ip is the IP address of the ESP8266 module, will be 
 *  printed to Serial when the module is connected.
 */

#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>

//const char* ssid = "abc";
//const char* password = "12345678";

const char* ssid = "SmartHome WLAN";
const char* password = "sh1234wlan";

//const char* ssid = "hundekuchen";
//const char* password = "P1zza=leckeR"; 


//UDP

WiFiUDP Udp;
unsigned int localUdpPort = 8888;
char incomingPacket[255];
char  replyPacket[] = "selfmade_smarthome";

int val;
  int d1, d2, d3, d4, d5, d6, d7, d8, d9 = 0;

// Create an instance of the server
// specify the port to listen on as an argument
WiFiServer server(80);

void setup() {
//  server.on("/", handleRootPath);
  Serial.begin(9600);
  delay(10);

  

  // prepare GPIO2
  // prepare GPIO
  pinMode(2, OUTPUT);
  pinMode(0, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(14, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(15, OUTPUT);
  pinMode(3, OUTPUT);
  //pinMode(1, OUTPUT);

  digitalWrite(2, 0);
  digitalWrite(0, 0);
  digitalWrite(4, 0);
  digitalWrite(5, 0);
  digitalWrite(14, 0);
  digitalWrite(12, 0);
  digitalWrite(13, 0);
  digitalWrite(15, 0);
  digitalWrite(3, 0);
  //digitalWrite(1, 0);
  
  
  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  
  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.println(WiFi.localIP());

  Udp.begin(localUdpPort);
  Serial.printf("Now listening at IP %s, UDP port %d\n", WiFi.localIP().toString().c_str(), localUdpPort);
}

void loop() {
  //UDP
  int packetSize = Udp.parsePacket();
  if (packetSize)
  {
    // receive incoming UDP packets
    Serial.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
    int len = Udp.read(incomingPacket, 255);
    if (len > 0)
    {
      incomingPacket[len] = 0;
    }
    Serial.printf("UDP packet contents: %s\n", incomingPacket);

    // send back a reply, to the IP address and port we got the packet from
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(replyPacket);
    Udp.endPacket();
  }
  
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  
  // Wait until the client sends some data
  Serial.println("new client");
  while(!client.available()){
    delay(1);
  }
  
  // Read the first line of the request
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();

  
  Serial.println();
  Serial.println("Anfrage:");
  Serial.print(req);
  Serial.println();

  // D1 GPIO Auslesen und setzen
  if (req.indexOf("d1/0") != -1){
      d1 = 0;
      //client.println("HTTP/1.0 200 OK");
      //client.print("test");
      digitalWrite(4, 0);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    } else if (req.indexOf("d1/1") != -1){
      d1 = 1;
      //client.println("HTTP/1.0 200 OK");
      //client.print("test");
      digitalWrite(4, 1);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    }

  // D2 GPIO04 Auslesen und setzen.
  else if (req.indexOf("d2/0") != -1){
      d2 = 0;
      //client.print("test");
      digitalWrite(5, 0);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    } else if (req.indexOf("d2/1") != -1){
      d2 = 1;
      digitalWrite(5, 1);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    }
    
   // D3 GPIO00 Auslesen und setzen.
  else if (req.indexOf("d3/0") != -1){
      d3 = 0;
      digitalWrite(0, 0);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    } else if (req.indexOf("d3/1") != -1){
      d3 = 1;
      digitalWrite(0, 1);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    }
    
    // D4 GPIO04 Auslesen und setzen.
  else if (req.indexOf("d4/0") != -1){
      d4 = 0;
      digitalWrite(2, 0);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    } else if (req.indexOf("d4/1") != -1){
      d4 = 1;
      digitalWrite(2, 1);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    }

    //Schaltet alle GPIOs an
    else if (req.indexOf ("allOn") != -1){
        d1 = 1;
        d2 = 1;
        d3 = 1;
        d4 = 1;
        digitalWrite(2, 1);
        digitalWrite(0, 1);
        digitalWrite(4, 1);
        digitalWrite(5, 1);
        client.flush();
        client.println("HTTP/1.0 200 OK");
      }

      //Schaltet alle GPIOs aus.
     else if (req.indexOf ("allOff") != -1){
        d1 = 0;
        d2 = 0;
        d3 = 0;
        d4 = 0;
        digitalWrite(2, 0);
        digitalWrite(0, 0);
        digitalWrite(4, 0);
        digitalWrite(5, 0);
        client.flush();
        client.println("HTTP/1.0 200 OK");
      }

    // D5 GPIO14 Auslesen und setzen.
    /*
  else if (req.indexOf("d5/0") != -1){
      d5 = 0;
      digitalWrite(14, 0);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    } else if (req.indexOf("d5/1") != -1){
      d5 = 1;
      digitalWrite(14, 1);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    }

    // D6 GPIO12 Auslesen und setzen.
  else if (req.indexOf("d6/0") != -1){
      d6 = 0;
      digitalWrite(12, 0);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    } else if (req.indexOf("d6/1") != -1){
      d6 = 1;
      digitalWrite(12, 1);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    }

    // D7 GPIO13 Auslesen und setzen.
  else if (req.indexOf("d7/0") != -1){
      d7 = 0;
      digitalWrite(13, 0);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    } else if (req.indexOf("d7/1") != -1){
      d7 = 1;
      digitalWrite(13, 1);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    }

    // D8 GPIO15 Auslesen und setzen.
  else if (req.indexOf("d8/0") != -1){
      d8 = 0;
      digitalWrite(15, 0);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    } else if (req.indexOf("d8/1") != -1){
      d8 = 1;
      digitalWrite(15, 1);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    }

    // D9 GPIO03 Auslesen und setzen.
  else if (req.indexOf("d9/0") != -1){
      d9 = 0;
      digitalWrite(3, 0);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    } else if (req.indexOf("d9/1") != -1){
      d9 = 1;
      digitalWrite(3, 1);
      client.println("HTTP/1.0 200 OK");
      client.flush();
    }
    */

    //Status der einzelnen GPIOs zurück geben.

    else if (req.indexOf("status") != -1){
      StaticJsonBuffer<1200> jsonBuffer;
      JsonObject& root = jsonBuffer.createObject();

      root["name"] = "Steckdosenleiste";

      JsonArray& state = root.createNestedArray("status");
      JsonObject& d1Status = state.createNestedObject();
      JsonObject& d2Status = state.createNestedObject();
      JsonObject& d3Status = state.createNestedObject();
      JsonObject& d4Status = state.createNestedObject();
      JsonObject& allOn = state.createNestedObject();
      JsonObject& allOff = state.createNestedObject();
      
        if(d1 == 0){
          d1Status["name"] = "Steckdose 1 Off";
          d1Status["state"] = 0;
          d1Status["toggle"] = WiFi.localIP().toString() + "/d1/1";
        } else {
          d1Status["name"] = "Steckdose 1 ON";
          d1Status["state"] = 1;
          d1Status["toggle"] = WiFi.localIP().toString() + "/d1/0";
        }
        if(d2 == 0){
          d2Status["name"] = "Steckdose 2 Off";
          d2Status["state"] = 0;
          d2Status["toggle"] = WiFi.localIP().toString() + "/d2/1";
        } else {
          d2Status["name"] = "Steckdose 2 On";
          d2Status["state"] = 1;
          d2Status["toggle"] = WiFi.localIP().toString() + "/d2/0";
        }
        if(d3 == 0){
          d3Status["name"] = "Steckdose 3 Off";
          d3Status["state"] = 0;
          d3Status["toggle"] = WiFi.localIP().toString() + "/d3/1";
        } else{
          d3Status["name"] = "Steckdose 3 On";
          d3Status["state"] = 1;
          d3Status["toggle"] = WiFi.localIP().toString() + "/d3/0";          
        }
        if(d4 == 0){
          d4Status["name"] = "Steckdose 4 Off";
          d4Status["state"] = 0;
          d4Status["toggle"] = WiFi.localIP().toString() + "/d4/1";
        } else {
          d4Status["name"] = "Steckdose 4 On";
          d4Status["state"] = 1;
          d4Status["toggle"] = WiFi.localIP().toString() + "/d4/0";
        }
        allOn["name"] = "Alle Einschalten";
        allOn["toggle"] = WiFi.localIP().toString() + "/allOn";

        allOff["name"] = "Alle Ausschalten";
        allOff["toggle"] = WiFi.localIP().toString() + "/allOff";

      client.println("HTTP/1.0 200 OK");
      client.println("Content-Type: application/json");
      client.println("Connection: close");
      client.println();
      root.printTo(Serial);
      root.printTo(client);
      client.flush();
              
      }

 
   else if (req.indexOf("info") != -1){
      StaticJsonBuffer<1250> jsonBuffer;
      JsonObject& root = jsonBuffer.createObject();

      
      
      //root["MacAddress"] = WiFi.macAddress();
      root["name"] = "Steckdosenleiste";


      //>>>>> JSON d1 <<<<<<
      //JSON d1 Einschalten
      JsonArray& functions = root.createNestedArray("functions");
      JsonObject& d1On = functions.createNestedObject();
      d1On["name"] = "Steckdose 1 Einschalten";
      d1On["url"] = WiFi.localIP().toString() + "/d1/1";
      
      //JSON d1 Ausschalten
      JsonObject& d1Off = functions.createNestedObject();
      d1Off["name"] = "Steckdose 1 Ausschalten";
      d1Off["url"] = WiFi.localIP().toString() + "/d1/0";


      //>>>>> JSON d2 <<<<<<
      //JSON d2 Einschalten
      JsonObject& d2On = functions.createNestedObject();
      d2On["name"] = "Steckdose 2 Einschalten";
      d2On["url"] = WiFi.localIP().toString() + "/d2/1";
      
      //JSON d2 Ausschalten
      JsonObject& d2Off = functions.createNestedObject();
      d2Off["name"] = "Steckdose 2 Aussschalten";
      d2Off["url"] = WiFi.localIP().toString() + "/d2/0";


      //>>>>> JSON d3 <<<<<<
      //JSON d3 Einschalten
      JsonObject& d3On = functions.createNestedObject();
      d3On["name"] = "Steckdose 3 Einschalten";
      d3On["url"] = WiFi.localIP().toString() + "/d3/1";
      
      //JSON d3 Ausschalten
      JsonObject& d3Off = functions.createNestedObject();
      d3Off["name"] = "Steckdose 3 Ausschalten";
      d3Off["url"] = WiFi.localIP().toString() + "/d3/0";


      //>>>>> JSON d4 <<<<<<
      //JSON d4 Einschalten
      JsonObject& d4On = functions.createNestedObject();
      d4On["name"] = "Steckdose 4 Einschalten";
      d4On["url"] = WiFi.localIP().toString() + "/d4/1";
      
      //JSON d4 Ausschalten
      JsonObject& d4Off = functions.createNestedObject();
      d4Off["name"] = "Steckdose 4 Ausschalten";
      d4Off["url"] = WiFi.localIP().toString() + "/d4/0";

      JsonObject& allOn = functions.createNestedObject();
      allOn["name"] = "Alle Steckdosen einschlten";
      allOn["url"] = WiFi.localIP().toString() + "/allOn";

      JsonObject& allOff = functions.createNestedObject();
      allOff["name"] = "Alle Steckdosen ausschalten";
      allOff["url"] = WiFi.localIP().toString() + "/allOff";

    /*
      //>>>>> JSON d5 <<<<<<
      //JSON d5 Einschalten
      JsonObject& d5On = functions.createNestedObject();
      d5On["name"] = "Steckdose 5 Einschalten";
      d5On["url"] = WiFi.localIP().toString() + "/d5/1";
      
      //JSON d5 Ausschalten
      JsonObject& d5Off = functions.createNestedObject();
      d5Off["name"] = "Steckdose 5 Ausschalten";
      d5Off["url"] = WiFi.localIP().toString() + "/d5/0";


      //>>>>> JSON d6 <<<<<<
      //JSON d6 Einschalten
      JsonObject& d6On = functions.createNestedObject();
      d6On["name"] = "Steckdose 6 Einschalten";
      d6On["url"] = WiFi.localIP().toString() + "/d6/1";
      
      //JSON d6 Ausschalten
      JsonObject& d6Off = functions.createNestedObject();
      d6Off["name"] = "Steckose 6 Aussschalten";
      d6Off["url"] = WiFi.localIP().toString() + "/d6/0";


      //>>>>> JSON d7 <<<<<<
      //JSON d7 Einschalten
      JsonObject& d7On = functions.createNestedObject();
      d7On["name"] = "Steckdose 7 Einschalten";
      d7On["url"] = WiFi.localIP().toString() + "/d7/1";
      
      //JSON d7 Ausschalten
      JsonObject& d7Off = functions.createNestedObject();
      d7Off["name"] = "Steckdose 7 Ausschalten";
      d7Off["url"] = WiFi.localIP().toString() + "/d7/0";


      //>>>>> JSON d8 <<<<<<
      //JSON d8 Einschalten
      JsonObject& d8On = functions.createNestedObject();
      d8On["name"] = "Steckdose 8 Einschalten";
      d8On["url"] = WiFi.localIP().toString() + "/d8/1";
      
      //JSON d8 Ausschalten
      JsonObject& d8Off = functions.createNestedObject();
      d8Off["name"] = "Steckdose 8 Ausschalten";
      d8Off["url"] = WiFi.localIP().toString() + "/d8/0";
      */

      client.println("HTTP/1.0 200 OK");
      client.println("Content-Type: application/json");
      client.println("Connection: close");
      client.println();
      root.printTo(Serial);
      root.printTo(client);
      client.flush();
      //String s = "HTTP/1.1 200 OK\r\nContent-Type: application/json \r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\n";
      //s += root;
      //s += "</html>";
      //client.stop();
   }
  

    //falsche Eingabe else fall
    else {
    Serial.println("test request");
    
    client.stop();
    return;
  }
  
  // Match the request
  //int val;
  //if (req.indexOf("/gpio/0") != -1)
  //  val = 0;
  //else if (req.indexOf("/gpio/1") != -1)
  //  val = 1;
  //else {
  //  Serial.println("invalid request");
  //  client.stop();
  //  return;
  //}

  // Set GPIO2 according to the request
  //digitalWrite(2, val);



  
  
  client.flush();

  // Prepare the response
  //String s = "HTTP/1.1 200 OK\r\nContent-Type: application/json \r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\n";
  //s += (val)?"high":"low";
  //s += "</html>\n";

  // Send the response to the client
  //client.print(s);
  delay(1);
  Serial.println("Client disonnected");

  // The client will actually be disconnected 
  // when the function returns and 'client' object is detroyed
}
