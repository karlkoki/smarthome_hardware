import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


class testThisStuff {

    public static void main(String[] args) throws MalformedURLException, IOException, InterruptedException {

            int off = 1;
            int on = 0;

            //Alle aus für den Start
            new URL("http://192.168.178.23/d1/" +  on).openStream().close();
            new URL("http://192.168.178.25/d2/" +  off).openStream().close();
            new URL("http://192.168.178.25/d1/" +  off).openStream().close();
            new URL("http://192.168.178.25/d3/" +  off).openStream().close();
            new URL("http://192.168.178.25/d4/" +  off).openStream().close();

            TimeUnit.SECONDS.sleep(2);

            //Jedes Relais an und nach einer Sekunde wieder aus
            System.out.println("EinerESP an.");
            new URL("http://192.168.178.23/d1/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);
            System.out.println("EinerESP aus.");
            new URL("http://192.168.178.23/d1/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#1 an.");
            new URL("http://192.168.178.25/d1/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);
            System.out.println("ViereESP Relais#1 aus.");
            new URL("http://192.168.178.25/d1/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#2 an.");
            new URL("http://192.168.178.25/d2/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);
            System.out.println("ViereESP Relais#2 aus.");
            new URL("http://192.168.178.25/d2/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#3 an.");
            new URL("http://192.168.178.25/d3/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);
            System.out.println("ViereESP Relais#3 aus.");
            new URL("http://192.168.178.25/d3/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#4 an.");
            new URL("http://192.168.178.25/d4/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);
            System.out.println("ViereESP Relais#4 aus.");
            new URL("http://192.168.178.25/d4/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            //Alle Ralais an und danach wieder aus
            System.out.println("EinerESP an.");
            new URL("http://192.168.178.23/d1/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);


            System.out.println("ViereESP Relais#1 an.");
            new URL("http://192.168.178.25/d1/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);


            System.out.println("ViereESP Relais#2 an.");
            new URL("http://192.168.178.25/d2/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);


            System.out.println("ViereESP Relais#3 an.");
            new URL("http://192.168.178.25/d3/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);


            System.out.println("ViereESP Relais#4 an.");
            new URL("http://192.168.178.25/d4/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("EinerESP aus.");
            new URL("http://192.168.178.23/d1/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#1 aus.");
            new URL("http://192.168.178.25/d1/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#2 aus.");
            new URL("http://192.168.178.25/d2/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#3 aus.");
            new URL("http://192.168.178.25/d3/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#4 aus.");
            new URL("http://192.168.178.25/d4/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            //Alle Ralais an und danach wieder aus - jetzt rückwärts und andersherum aus
            System.out.println("ViereESP Relais#4 an.");
            new URL("http://192.168.178.25/d4/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#3 an.");
            new URL("http://192.168.178.25/d3/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#2 an.");
            new URL("http://192.168.178.25/d2/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#1 an.");
            new URL("http://192.168.178.25/d1/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("EinerESP an.");
            new URL("http://192.168.178.23/d1/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("EinerESP aus.");
            new URL("http://192.168.178.23/d1/" +  on).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#1 aus.");
            new URL("http://192.168.178.25/d1/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#2 aus.");
            new URL("http://192.168.178.25/d2/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#3 aus.");
            new URL("http://192.168.178.25/d3/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);

            System.out.println("ViereESP Relais#4 aus.");
            new URL("http://192.168.178.25/d4/" +  off).openStream().close();
            TimeUnit.SECONDS.sleep(1);


            /*
            //schnell zum ende
            new URL("http://192.168.178.23/d1/" +  off).openStream().close();
            new URL("http://192.168.178.25/d2/" +  on).openStream().close();
            new URL("http://192.168.178.25/d1/" +  on).openStream().close();
            new URL("http://192.168.178.25/d3/" +  on).openStream().close();
            new URL("http://192.168.178.25/d4/" +  on).openStream().close();

            new URL("http://192.168.178.23/d1/" +  on).openStream().close();
            new URL("http://192.168.178.25/d2/" +  off).openStream().close();
            new URL("http://192.168.178.25/d1/" +  off).openStream().close();
            new URL("http://192.168.178.25/d3/" +  off).openStream().close();
            new URL("http://192.168.178.25/d4/" +  off).openStream().close();
            */
    }
}