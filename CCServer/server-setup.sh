#!/bin/bash

WEBAPP_ROOT_DIR=/var/www/shsh
WEBAPP_DIR_OWNER=www-data
WEBAPP_DIR_GROUP=www-data

BASENAME=`basename $0`

USAGE="Verwendung: $BASENAME [OPTIONEN]
Installations-Skript für den Command & Control Server des SmartHome-Hardware Teams.

--check-requirements-only       Prüfe nur die Erfüllung der Systemanforderungen
--no-install                    Installiere keine Software-Pakete
-h, --help                      Gebe diesen Hilfs-Text aus"

for arg in "$@"; do
	case "$arg" in
		--check-requirements-only)
			CHECK_REQUIREMENTS_ONLY=1;;
		--no-install)
			NO_INSTALL=1;;
		--help)
			HELP=1;;
		-*)
			while getopts ":h" short_arg $arg; do
				case "$short_arg" in
					h)
						HELP=1;;
				esac
			done;;
	esac
done

CheckSystemRequirements() {
	echo ""
	echo "##################################"
	echo "# Prüfen der Systemanforderungen #"
	echo "##################################"

	# Sicherstellen, dass das Skript mit root-Rechten ausgeführt wird
	if [ `whoami` != "root" ]; then
		echo "[x] Skript muss mit Root-Superkräften ausgeführt werden - bitte erneut mit \"sudo server-setup.sh\" aufrufen"
		exit 1
	else
		echo "[+] Skript wird als Root ausgeführt - OK"
	fi

	if [ ! -f /etc/debian_version ]; then
		echo "[x] Das Skript wird nicht auf einem Debian System ausgeführt - Aktuell wird nur Debian unterstützt"
		exit 1
	else
		echo "[+] Skript wird auf einem Debian-basierten System ausgeführt - OK"
	fi

	if [ $(ping -q -w 1 -c 1 `ip r | grep default | cut -d ' ' -f 3` > /dev/null && echo ok || echo error) != "ok" ]; then
		echo "[x] Konnte keine Verbindung zum Internet herstellen"
		exit 1
	else
		echo "[+] Bestehende Internet-Verbindung - OK"
	fi

	echo "[*] Prüfung erfolgreich abgeschlossen"
}

InstallPackages() {
	echo ""
	echo "######################################"
	echo "# Installieren der benötigten Pakete #"
	echo "######################################"

	if [ "$NO_INSTALL" = 1 ]; then
		echo "[*] Überspringe die Installation von Software-Paketen"
		return
	fi

	# Aktualisieren der Paketquellen
	apt update

	# Installieren der Pakete
	apt install -y apache2 sqlite3 php7.0 php7.0-sqlite3 php7.0-mbstring git

	# Composer installieren
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
	php composer-setup.php --install-dir=/usr/bin --filename=composer
	php -r "unlink('composer-setup.php');"

	echo "[*] Installation der benötigten Pakete abgeschlossen"
}

DeployWebApp() {
	echo ""
	echo "##################################"
	echo "# Installieren der Web-Anwendung #"
	echo "##################################"

	echo "[*] Lade Quellcode herunter..."
	# Clone das Git-Repository des Projekts und kopiere die Website-Dateien nach $WEBAPP_ROOT_DIR
	#git clone https://stash.iue.fh-kiel.de/scm/aem/smarthome_hardware.git
	git clone https://gitlab.com/karlkoki/smarthome_hardware.git
	
	echo "[*] Kopiere Daten für die Web-Anwendung nach $WEBAPP_ROOT_DIR"
	mkdir -p $WEBAPP_ROOT_DIR
	cp -r ./smarthome_hardware/CCServer/Site/. $WEBAPP_ROOT_DIR/
	cp ./smarthome_hardware/README.md $WEBAPP_ROOT_DIR/src/help.md

	echo "[*] Räume auf..."
	rm -rf ./smarthome_hardware

	echo "[*] Initialisiere die Datenbank"
	cd $WEBAPP_ROOT_DIR
	rm ccserver.db
	sqlite3 ccserver.db < ccserver.db.sql

	echo "[*] Installiere notwendige PHP Dependencies"
	composer install
	composer dump-autoload -o

	echo "[*] Korrigiere Zugriffsrechte von $WEBAPP_ROOT_DIR"
	
	# Setze den Besitzer und die Besitzer-Gruppe
	chown -c -R "$WEBAPP_DIR_OWNER":"$WEBAPP_DIR_GROUP" "$WEBAPP_ROOT_DIR" > /dev/null

	# Setze die Zugriffsrechte:
	#   User: read, write, execute
	#   Group: read, execute
	#   Others: execute
	chmod 755 -c -R "$WEBAPP_ROOT_DIR" > /dev/null

	if [ -f /etc/apache2/sites-enabled/000-default.conf ]; then
		echo "[!] Apache Konfiguration für die default Site ist aktiviert. Deaktivieren..."
		rm /etc/apache2/sites-enabled/000-default.conf
	fi

	echo "[*] Einrichten der Apache Site"
	sed -e "s#\${SHSH_DOCUMENT_ROOT}#$WEBAPP_ROOT_DIR/src#" ./apache-site.conf > /etc/apache2/sites-available/shsh.conf
	ln -f -s /etc/apache2/sites-available/shsh.conf /etc/apache2/sites-enabled/shsh.conf
	rm -f ./apache-site.conf

	echo "[*] Apache neustarten..."
	service apache2 reload

	echo "[*] Web-Anwendung erfolgreich installiert - sie ist unter http://127.0.0.1/ erreichbar"
}

if [ "$HELP" = 1 ]; then
	echo "$USAGE"
	exit 0
fi

CheckSystemRequirements

if [ "$CHECK_REQUIREMENTS_ONLY" = 1 ]; then
		exit 0
fi

InstallPackages

DeployWebApp
