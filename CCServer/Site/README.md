# CCServer Site

Diese README soll nur kurz beschreiben, wie man die CCServer Site lokal auf seinem Rechner zum laufen bekommt.

Die Anleitung für den Endnutzer ist der globalen README im Root-Verzeichnis des Repositories zu entnehmen.

## Voraussetzungen

* [PHP >= 7](https://secure.php.net/downloads.php) mit [aktivierter sqlite Extension](https://secure.php.net/manual/de/sqlite.installation.php)
* [Composer](https://getcomposer.org/download/)

## Nutzung

* Installation der Dependencies: `composter install`
* Generieren der Autoload-Dateien: `composer dump-autoload -o`
* Starten eines lokalen PHP Servers: `php -S localhost:8080 -t src`
* Aufrufen von `http://localhost:8080/` im Browser