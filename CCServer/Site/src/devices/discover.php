<?php

require_once __DIR__ . "/../../vendor/autoload.php";

use SMSM\Devices\DeviceDiscovery;
use SMSM\Devices\DeviceFunctionRepository;
use SMSM\Devices\DeviceRepository;

$discovery = new DeviceDiscovery(new DeviceRepository(), new DeviceFunctionRepository());

//$discovered_devices = [Device::fromJson(file_get_contents(__DIR__ . "/../../info.json"), "AA:BB:CC:DD:EE:FG", "121.111.111.111")];
$discovered_devices = array_filter(
    array_map(
        array($discovery, "gatherDeviceInfo"),
        $discovery->discoverDevices()
    ), function ($deviceInfo) {
    return $deviceInfo !== false;
});

if ($_SERVER["REQUEST_METHOD"] === "GET") {
    if (empty($discovered_devices)) {
        // Hier gibt's nichts zu sehen, wir Leiten den Client zurück zu /devices/index.php
        http_response_code(204);
        header("Location: index.php");
    } else {
        // Speichern oder Aktualisieren der gefundenen Geräte
        if (isset($_GET["persist"]) && $_GET["persist"] === "true") {
            // Übernehme die Gerätedaten nur in die Datenbank, wenn das
            // Skript mit dem Query Parameter persist=true aufgerufen wurde
            $discovery->saveOrUpdateDevicesInDb($discovered_devices);
        }

        http_response_code(200);

        if ($_SERVER["HTTP_ACCEPT"] === "application/json") {
            // Client erwartet explizit eine JSON Response
            header("Content-Type: application/json");

            echo json_encode($discovered_devices);
        } else {
            // Client ist wahrscheinlich ein Browser und kommt von /devices/index.php,
            // also leiten wir ihn dorthin zurück.
            header("Location: index.php");
        }
    }
} else {
    // Andere Methoden als GET sind nicht erlaubt
    http_response_code(405);
}

