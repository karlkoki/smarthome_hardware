<?php

namespace SMSM\Devices;

class DeviceRepository
{

    const DATABASE_FILE_PATH = __DIR__ . "/../../ccserver.db";

    /**
     * @return Device[]
     */
    public function findAll()
    {
        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READONLY);

        $query = $db->query("SELECT * FROM hardware;");

        $devices = array();

        while ($result_row = $query->fetchArray(SQLITE3_ASSOC)) {
            array_push($devices, $this->mapRowToClass($result_row));
        }

        $db->close();

        return $devices;
    }

    /**
     * @param int $deviceId
     * @return Device|null
     */
    public function findById(int $deviceId)
    {
        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READONLY);

        $query_stmt = $db->prepare("SELECT * FROM hardware WHERE id = :ID;");
        $query_stmt->bindValue("ID", $deviceId);

        $device = NULL;

        if ($result_row = $query_stmt->execute()->fetchArray(SQLITE3_ASSOC)) {
            $device = $this->mapRowToClass($result_row);
        }

        $db->close();

        return $device;
    }

    /**
     * @param string $macAddress
     * @return null|Device
     */
    public function findByMacAddress(string $macAddress)
    {
        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READONLY);

        $query_stmt = $db->prepare("SELECT * FROM hardware WHERE MAC = :MAC;");
        $query_stmt->bindValue("MAC", $macAddress);

        $device = NULL;

        if ($result_row = $query_stmt->execute()->fetchArray(SQLITE3_ASSOC)) {
            $device = $this->mapRowToClass($result_row);
        }

        $db->close();

        return $device;
    }

    /**
     * @param Device $device
     * @return bool
     */
    public function update(Device $device)
    {
        if ($device->getId() === NULL) {
            return false;
        }

        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READWRITE);

        $update_stmt = $db->prepare("UPDATE hardware SET name = :NAME, location_id = :LOCATION_ID, MAC = :MAC, IP = :IP, deleted = :DELETED WHERE id = :ID;");
        $update_stmt->bindValue("NAME", $device->getName());
        $update_stmt->bindValue("LOCATION_ID", $device->getLocationId());
        $update_stmt->bindValue("MAC", $device->getMacAddress());
        $update_stmt->bindValue("IP", $device->getIpAddress());
        $update_stmt->bindValue("DELETED", $device->isDeleted());
        $update_stmt->bindValue("ID", $device->getId());

        $result = $update_stmt->execute() !== false;

        $db->close();

        return $result;
    }

    /**
     * @param Device $device
     * @return bool|int
     */
    public function insert(Device $device)
    {
        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READWRITE);

        $insert_stmt = $db->prepare("INSERT INTO hardware (name, location_id, IP, MAC, deleted) VALUES (:NAME, :LOCATION_ID, :IP, :MAC, :DELETED);");
        $insert_stmt->bindValue("NAME", $device->getName());
        $insert_stmt->bindValue("LOCATION_ID", $device->getLocationId());
        $insert_stmt->bindValue("IP", $device->getIpAddress());
        $insert_stmt->bindValue("MAC", $device->getMacAddress());
        $insert_stmt->bindValue("DELETED", $device->isDeleted());

        $result = ($insert_stmt->execute() !== false) ? $db->lastInsertRowID() : false;

        $db->close();

        return $result;
    }

    /**
     * @param array $db_row
     * @return Device
     */
    private function mapRowToClass(array $db_row)
    {
        $device = new Device(
            $db_row["name"],
            $db_row["MAC"],
            $db_row["IP"],
            array()
        );

        $device->setId(intval($db_row["id"]));

        if (isset($db_row["location_id"])) {
            $device->setLocationId(intval($db_row["location_id"]));
        }

        if (isset($db_row["deleted"])) {
            $device->setDeleted((bool)$db_row["deleted"]);
        }

        return $device;
    }

}