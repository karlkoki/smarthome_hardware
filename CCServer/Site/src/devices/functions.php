<?php

require_once __DIR__ . "/../../vendor/autoload.php";

use SMSM\Devices\DeviceFunction;
use SMSM\Devices\DeviceFunctionRepository;

$httpClient = new GuzzleHttp\Client(["verify" => false]);
$deviceFunctionRepository = new DeviceFunctionRepository();

/**
 * @param DeviceFunction $function
 * @return string
 */
function getFunctionUrl(DeviceFunction $function)
{
    return $function->getUrl();
}

/**
 * @param DeviceFunction $function
 * @return bool
 */
function isAllOnFunction(DeviceFunction $function)
{
    return strpos(strtolower($function->getUrl()), "allon") !== false;
}

/**
 * @param DeviceFunction $function
 * @return bool
 */
function isAllOffFunction(DeviceFunction $function)
{
    return strpos(strtolower($function->getUrl()), "alloff") !== false;
}

/**
 * @param string[]           $urls
 * @param \GuzzleHttp\Client $httpClient
 */
function fireAndForget(array $urls, GuzzleHttp\Client $httpClient)
{
    $promises = array_map(function ($url) use ($httpClient) {
        return $httpClient->getAsync($url);
    }, $urls);

    try {
        // Warte, bis alle Requests abgearbeitet sind
        \GuzzleHttp\Promise\settle($promises)->wait();
    } catch (GuzzleHttp\Exception\RequestException $e) {
        // Don't care
    }
}

/**
 * @param DeviceFunctionRepository $deviceFunctionRepository
 * @param \GuzzleHttp\Client       $httpClient
 */
function enableAllFunctions(DeviceFunctionRepository $deviceFunctionRepository, \GuzzleHttp\Client $httpClient)
{
    fireAndForget(array_map(
        "getFunctionUrl",
        array_filter(
            $deviceFunctionRepository->findAll(),
            "isAllOnFunction"
        )
    ), $httpClient);
}

/**
 * @param DeviceFunctionRepository $deviceFunctionRepository
 * @param \GuzzleHttp\Client       $httpClient
 */
function disableAllFunctions(DeviceFunctionRepository $deviceFunctionRepository, \GuzzleHttp\Client $httpClient)
{
    fireAndForget(array_map(
        "getFunctionUrl",
        array_filter(
            $deviceFunctionRepository->findAll(),
            "isAllOffFunction"
        )
    ), $httpClient);
}

/**
 * @param int                      $functionId
 * @param DeviceFunctionRepository $deviceFunctionRepository
 * @param \GuzzleHttp\Client       $httpClient
 * @return bool
 */
function triggerFunction(int $functionId, DeviceFunctionRepository $deviceFunctionRepository, \GuzzleHttp\Client $httpClient) {
    if (($function = $deviceFunctionRepository->findById($functionId)) !== NULL) {
        fireAndForget([$function->getUrl()], $httpClient);

        return true;
    } else {
        return false;
    }
}

switch ($_SERVER["REQUEST_METHOD"]) {
    case "GET":
        if (isset($_GET["enable_all"])) {
            enableAllFunctions($deviceFunctionRepository, $httpClient);
        } else if (isset($_GET["disable_all"])) {
            disableAllFunctions($deviceFunctionRepository, $httpClient);
        } else if (isset($_GET["trigger"]) && is_numeric($_GET["trigger"])) {
            if (triggerFunction(intval($_GET["trigger"]), $deviceFunctionRepository, $httpClient)) {
                http_response_code(200);
            } else {
                http_response_code(404);
            }
        } else {
            http_response_code(400);
        }
        break;
    default:
        http_response_code(405);
        break;
}
