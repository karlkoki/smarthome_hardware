<?php

namespace SMSM\Devices;

class Device implements \JsonSerializable
{

    private $id;

    private $name;

    private $macAddress;

    private $ipAddress;

    private $locationId;

    private $deleted = false;

    private $functions;

    /**
     * @param string           $name Name des Geräts
     * @param string           $macAddress MAC-Adresse des Geräts
     * @param string           $ipAddress IP-Adresse des Geräts
     * @param DeviceFunction[] $functions Funktionen des Geräts
     */
    public function __construct(string $name, string $macAddress, string $ipAddress, array $functions)
    {
        $this->name = $name;
        $this->macAddress = $macAddress;
        $this->ipAddress = $ipAddress;
        $this->functions = $functions;
    }

    /**
     * @param string $json
     * @param string $macAddress
     * @param string $ipAddress
     * @return Device
     */
    static function fromJson(string $json, string $macAddress = "", string $ipAddress = "")
    {
        $json_data = json_decode($json, true);

        if (empty($macAddress) && isset($json_data["macAddress"])) {
            $macAddress = $json_data["macAddress"];
        }

        if (empty($ipAddress) && isset($json_data["ipAddress"])) {
            $ipAddress = $json_data["ipAddress"];
        }

        $funcs = array_map("SMSM\Devices\DeviceFunction::fromJson", $json_data["functions"]);

        return new Device($json_data["name"], $macAddress, $ipAddress, $funcs);
    }

    /**
     * @param Device $device
     * @return bool
     */
    static function isNotDeleted(Device $device) {
        return $device->isDeleted() !== NULL && $device->isDeleted() !== true;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getMacAddress()
    {
        return $this->macAddress;
    }

    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     */
    public function setIpAddress(string $ipAddress)
    {
        $this->ipAddress = $ipAddress;
    }

    /**
     * @return int|null
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    public function setLocationId(int $locationId)
    {
        $this->locationId = $locationId;
    }

    /**
     * @return null|bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return DeviceFunction[]
     */
    public function getFunctions()
    {
        return $this->functions;
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return array(
            "id" => $this->getId(),
            "name" => $this->getName(),
            "macAddress" => $this->getMacAddress(),
            "ipAddress" => $this->getIpAddress(),
            "locationId" => $this->getLocationId(),
            "deleted" => $this->isDeleted(),
            "functions" => $this->getFunctions()
        );
    }

    public function __toString()
    {
        return sprintf("Device{name=%s, macAddress=%s, ipAddress=%s locationId=%s, deleted=%s, functions=[%s]}",
            $this->name, $this->macAddress, $this->ipAddress, $this->getLocationId(), $this->isDeleted(),
            implode(", ", $this->functions));
    }

}