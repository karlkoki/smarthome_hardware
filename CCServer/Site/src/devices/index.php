<?php

require_once __DIR__ . "/../../vendor/autoload.php";

use SMSM\Devices\Device;
use SMSM\Devices\DeviceFunctionRepository;
use SMSM\Devices\DeviceRepository;
use SMSM\Locations\LocationRepository;

$deviceRepository = new DeviceRepository();
$deviceFunctionRepository = new DeviceFunctionRepository();
$locationRepository = new LocationRepository();

/**
 * Generiere HTML Code für ein gegebenes Gerät, in dem dessen Funktionen
 * in einer Tablle aufgelistet und anklickbar dargestellt werden.
 *
 * @param Device                   $device
 * @param DeviceFunctionRepository $deviceFunctionRepository
 * @return array
 */
function generateModalForDevice(Device $device, DeviceFunctionRepository $deviceFunctionRepository)
{
    $modal_html = "<div class=\"modal fade\" id=\"modal_" . $device->getId() . "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">";
    $modal_html .= "    <div class=\"modal-dialog\" role=\"document\">";
    $modal_html .= "        <div class=\"modal-content\">";
    $modal_html .= "            <div class=\"modal-header\">";
    $modal_html .= "                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>";
    $modal_html .= "                <h4 class=\"modal-title\" id=\"myModalLabel\">" . $device->getName() . "</h4>";
    $modal_html .= "            </div>";
    $modal_html .= "            <div class=\"modal-body\">";
    $modal_html .= "                <table class=\"table\">";
    $modal_html .= "                <thead>";
    $modal_html .= "                    <th>#</th>";
    $modal_html .= "                    <th>Name</th>";
    $modal_html .= "                </thead>";
    $modal_html .= "                <tbody>";

    foreach ($deviceFunctionRepository->findByHardwareId($device->getId()) as $fun) {
        $modal_html .= "<tr>";
        $modal_html .= "<td>" . $fun->getId() . "</td>";
        $modal_html .= "<td><a href='#' onclick='triggerFunction(" . $fun->getId() . ")'><span class=\"glyphicon glyphicon-flash\" aria-hidden=\"true\"></span>" . htmlentities($fun->getName()) . "</a></td>";
        $modal_html .= "</tr>";
    }

    $modal_html .= "                </tbody>";
    $modal_html .= "            </table>";
    $modal_html .= "            </div>";
    $modal_html .= "            <div class=\"modal-footer\">";
    $modal_html .= "                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Schließen</button>";
    $modal_html .= "            </div>";
    $modal_html .= "        </div>";
    $modal_html .= "    </div>";
    $modal_html .= "</div>";

    return array(
        "modal" => $modal_html,
        "modal_id" => "modal_" . $device->getId()
    );
}

// Lade alle Geräte aus der Datenbank
$devices = array_filter($deviceRepository->findAll(), ["SMSM\Devices\Device", "isNotDeleted"]);

?>

<!doctype html>
<html>
<head>
    <title>Geräte :: SMSM - Selfmade SmartHome</title>
    <link rel=stylesheet type=text/css href="/static/css/bootstrap.css">
    <link rel=stylesheet type=text/css href="/static/css/style.css">
    <link rel="icon" type="image/png" href="/static/img/favicon.png">
    <script type="text/javascript" rel="script">
        function triggerFunction(functionId) {
            $.ajax("/devices/functions.php?trigger=" + functionId)
                .done(function () {
                    console.log("Triggered function " + functionId);
                });
        }

        function enableAllFunctions() {
            $.ajax("/devices/functions.php?enable_all")
                .done(function() {
                    console.log("Enabled all functions");
                });
        }

        function disableAllFunctions() {
            $.ajax("/devices/functions.php?disable_all")
                .done(function() {
                    console.log("Disabled all functions");
                });
        }
    </script>
</head>
<body>
<!-- The upper navbar with logo and navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Left section with logo -->
        <div class="navbar-left">
            <a href="/" class="navbar-brand navbar-logo">
                <img src="/static/img/logo.svg"/>
                Selfmade SmartHome
            </a>
        </div>

        <!-- Right section with navigation -->
        <nav class="navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="/">Startseite</a></li>
                <li><a href="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">Geräte</a></li>
                <li><a href="#">Impressum</a></li>
                <li><a href="/help.php">Hilfe</a></li>
                <li><a href="/contact.php">Kontakt</a></li>
            </ul>
        </nav>
    </div>
</nav>

<div class="container main-container">
    <div class="page-header">
        <h1>Geräte</h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">Verfügbare Geräte</div>

        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Standort</th>
                <th>IP-Adresse</th>
                <th>MAC-Adresse</th>
                <th>Optionen</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($devices as $device) {
                $location = "Unbekannt";
                if ($device->getLocationId() !== NULL) {
                    $location = $locationRepository->findById($device->getLocationId())->getName();
                }

                echo "<tr>";
                echo "<td>" . htmlentities($device->getId()) . "</td>";
                echo "<td>" . htmlentities($device->getName()) . "</td>";
                echo "<td>" . htmlentities($location) . "</td>";
                echo "<td>" . htmlentities($device->getIpAddress()) . "</td>";
                echo "<td>" . htmlentities($device->getMacAddress()) . "</td>";
                echo "<td>";
                echo "    <a href=\"#\" title=\"Funktionen\" class=\"btn btn-default btn-md\" data-toggle=\"modal\" data-target=\"#modal_" . $device->getId() . "\">";
                echo "        <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>";
                echo "    </a>";
                echo "    <a href=\"/devices/edit.php?id=" . $device->getId() . "\" title=\"Bearbeiten\" class=\"btn btn-default btn-md\">";
                echo "        <span class=\"glyphicon glyphicon-edit\" aria-hidden=\"true\"></span>";
                echo "    </a>";
                echo "    <a href=\"/devices/delete.php?id=" . $device->getId() . "\" title=\"Löschen\" class=\"btn btn-danger btn-md\">";
                echo "        <span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span>";
                echo "    </a>";
                echo "</tr>";
            }
            ?>
            </tbody>
        </table>
    </div>

    <div style="float: left !important;">
        <a href="/devices/discover.php?persist=true" class="btn btn-primary btn-mg">
            <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
            Nach Geräten im Netzwerk suchen
        </a>
    </div>

    <div style="float: right !important;">
        <a href="#" onclick="enableAllFunctions()" class="btn btn-warning btn-mg">
            <span class="glyphicon glyphicon-play" aria-hidden="true"></span>
            Alle Funktionen aktivieren
        </a>
        <a href="#" onclick="disableAllFunctions()" class="btn btn-danger btn-mg">
            <span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
            Alle Funktionen deaktivieren
        </a>
    </div>

    <?php
    foreach ($devices as $device) {
        echo generateModalForDevice($device, $deviceFunctionRepository)["modal"];
    }
    ?>
</div>

<script src="/static/js/jquery.js"></script>
<script src="/static/js/bootstrap.min.js"></script>
</body>
</html>