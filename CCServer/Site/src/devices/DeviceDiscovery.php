<?php

namespace SMSM\Devices;

use GuzzleHttp;

class DeviceDiscovery
{

    const DEVICE_DISCOVERY_MSG = "DEVICE_DISCOVERY";

    const DEVICE_DISCOVERY_BROADCAST_IPV4 = "255.255.255.255";

    const DEVICE_DISCOVERY_PORT = "8888";

    const DEVICE_DISCOVERY_TIMEOUT = [
        "sec" => 5,
        "usec" => 0
    ];

    const DEVICE_DISCOVERY_EXPECTED_MSG = "selfmade_smarthome";

    const REGEX_MAC_ADDRESS = "/([0-9A-F]{1,2}[:-]){5}([0-9A-F]{2})/i";

    const DATABASE_FILE_PATH = __DIR__ . "/../../ccserver.db";

    private $deviceRepository;

    private $deviceFunctionRepository;

    /**
     * DeviceDiscovery constructor.
     * @param DeviceRepository         $deviceRepository
     * @param DeviceFunctionRepository $deviceFunctionRepository
     */
    public function __construct(DeviceRepository $deviceRepository, DeviceFunctionRepository $deviceFunctionRepository)
    {
        $this->deviceRepository = $deviceRepository;
        $this->deviceFunctionRepository = $deviceFunctionRepository;
    }


    /**
     * Suche nach kompatiblen Geräten im Netzwerk.
     *
     * @return string[] Ein Array mit IPv4 Adressen aller gefundenen Geräte
     */
    public function discoverDevices()
    {
        // Erzeuge einen Socket über IPv4 für UDP Verbindungen
        $socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);

        // Aktiviere die Unterstützung für Broadcast-Nachrichten für den Socket
        socket_set_option($socket, SOL_SOCKET, SO_BROADCAST, true);

        // Setze das Timeout für Verbindungen
        socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, self::DEVICE_DISCOVERY_TIMEOUT);

        socket_sendto($socket, self::DEVICE_DISCOVERY_MSG, strlen(self::DEVICE_DISCOVERY_MSG), 0, self::DEVICE_DISCOVERY_BROADCAST_IPV4, self::DEVICE_DISCOVERY_PORT);

        $devices = array();

        while (true) {
            $response = socket_recvfrom($socket, $buffer, 20, 0, $device_ipv4, $device_port);

            if ($response === false) {
                break;
            } else {
                if ($buffer === self::DEVICE_DISCOVERY_EXPECTED_MSG) {
                    array_push($devices, $device_ipv4);
                }
            }
        }

        socket_close($socket);
        return $devices;
    }

    /**
     * Frage mithilfe des ARP-Protokolls die MAC-Adresse eines Gerätes auf Basis
     * seiner IPv4-Adresse ab. Die MAC-Adresse ist garantiert eindeutig und kann
     * deshalb als Identifikator für Geräte verwendet werden.
     *
     * @param string $ipv4Address
     * @return string|bool Die entsprechende MAC-Adresse als string oder false, wenn die Abfrage
     * der MAC-Adresse nicht geklappt haben sollte
     */
    function getMacAddressForIPv4(string $ipv4Address)
    {
        $arp_output = shell_exec(sprintf("arp -n %s", $ipv4Address));

        if ($arp_output === NULL) {
            return false;
        } else {
            if (preg_match(self::REGEX_MAC_ADDRESS, $arp_output, $regex_matches) === 1) {
                return trim(strtoupper($regex_matches[0]));
            } else {
                return false;
            }
        }
    }

    /**
     * Frage weitere Informationen von einem als kompatibel identifizierten Gerät ab.
     *
     * @param string $device_addr IPv4 Adresse des Geräts
     * @return Device|bool
     */
    public function gatherDeviceInfo(string $device_addr)
    {
        $http_client = new GuzzleHttp\Client(["verify" => false]);

        try {
            $response = $http_client->get("http://" . $device_addr . "/info");
        } catch (GuzzleHttp\Exception\RequestException $e) {
            return false;
        }

        if ($response->getStatusCode() === 200) {
            if ($response->getHeader("Content-Type")[0] === "application/json") {
                return Device::fromJson(
                    $response->getBody()->getContents(),
                    $this->getMacAddressForIPv4($device_addr),
                    $device_addr
                );
            }
        }

        return false;
    }

    /**
     * Speichere oder aktualisiere Gerätedaten in der Datenbank.
     *
     * @param Device[] $devices
     */
    public function saveOrUpdateDevicesInDb(array $devices)
    {
        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READWRITE);

        foreach ($devices as $device) {
            $this->saveOrUpdateDeviceInDb($db, $device);
        }

        $db->close();
    }

    /**
     * @param \SQLite3 $db Die Datenbank, die für die Operation verwendet werden soll
     * @param Device   $device Das zu speichernde Gerät
     */
    private function saveOrUpdateDeviceInDb(\SQLite3 $db, Device $device)
    {
        // Prüfe, ob es bereits ein Gerät mit derselben MAC-Adresse gibt
        $existing_device = $this->deviceRepository->findByMacAddress($device->getMacAddress());

        if ($existing_device !== NULL) {
            // Es existiert bereits ein Gerät mit identischer MAC-Adresse.
            // Wir aktualisieren die IP-Adresse, falls diese sich geändert hat.
            if ($existing_device->getIpAddress() !== $device->getIpAddress()) {
                $existing_device->setIpAddress($device->getIpAddress());

                $this->deviceRepository->update($existing_device);

                // TODO Funktionen aktualisieren
            }
        } else {
            // Es existiert noch kein Gerät mit identischer MAC-Adresse, wir können
            // also stupide alle Daten in die Datenbank einfügen.
            $device_id = $this->deviceRepository->insert($device);

            foreach ($device->getFunctions() as $fun) {
                $this->deviceFunctionRepository->insert($fun, $device_id);
            }
        }
    }

}
