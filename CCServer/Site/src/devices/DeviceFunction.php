<?php

namespace SMSM\Devices;

class DeviceFunction implements \JsonSerializable
{

    private $id;

    private $name;

    private $url;

    /**
     * @param string $name Name der Funktion
     * @param string $url URL, unter der die Funktion getriggert werden kann
     */
    public function __construct(string $name, string $url)
    {
        $this->name = $name;
        $this->url = $url;
    }

    /**
     * @param $json
     * @return DeviceFunction
     */
    public static function fromJson($json)
    {
        $url = htmlentities($json["url"]);

        if (strpos($url, "http://") === false && strpos($url, "https://") === false) {
            // Sollte dir URL noch kein Protokoll enthalten, füge standardmäßig das HTTP Protokoll an
            $url = "http://" . $url;
        }

        return new DeviceFunction($json["name"], $url);
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return array(
            "name" => $this->getName(),
            "url" => $this->getUrl()
        );
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "DeviceFunction{name=" . $this->name . ", url=" . $this->url . "}";
    }

}