<?php

require_once __DIR__ . "/../../vendor/autoload.php";

use SMSM\Devices\DeviceRepository;
use SMSM\Devices\DeviceFunctionRepository;

/**
 * Lösche ein gegebenes Gerät.
 *
 * Das Gerät selbst wird in diesem Fall nur als gelöscht MARKIERT, damit es nicht
 * beim nächsten Aufruf der Autodiscovery wieder auftauchen kann. Die Funktionen
 * des Gerätes hingegen werden tatsächlich alle aus der Datenbank gelöscht.
 *
 * @param int                      $deviceId
 * @param DeviceRepository         $deviceRepository
 * @param DeviceFunctionRepository $deviceFunctionRepository
 * @return bool
 */
function deleteDevice(int $deviceId, DeviceRepository $deviceRepository, DeviceFunctionRepository $deviceFunctionRepository)
{
    if (($device = $deviceRepository->findById($deviceId)) !== NULL) {
        // Markiere das Gerät als gelöscht
        $device->setDeleted(true);
        $update_result = $deviceRepository->update($device);

        // Lösche alle Funktionen des Geräts
        return $update_result && $deviceFunctionRepository->deleteByHardwareId($deviceId);
    } else {
        return false;
    }
}

switch ($_SERVER["REQUEST_METHOD"]) {
    case "GET":
        if (isset($_GET["id"]) && is_numeric($_GET["id"])) {
            if (deleteDevice(intval($_GET["id"]), new DeviceRepository(), new DeviceFunctionRepository())) {
                http_response_code(200);
            } else {
                http_response_code(500);
            }
        } else {
            http_response_code(400);
        }

        if (isset($_SERVER["HTTP_REFERER"])) {
            http_response_code(302);
            header("Location: " . $_SERVER["HTTP_REFERER"]);
        }
        break;
    default:
        http_response_code(405);
        break;
}