<?php

require_once __DIR__ . "/../../vendor/autoload.php";

use SMSM\Devices\DeviceRepository;
use SMSM\Locations\LocationRepository;

$deviceRepository = new DeviceRepository();
$locationRepository = new LocationRepository();

function generateButtonBackToDeviceOverviewForOccasion(string $occasion)
{
    echo "<div class=\"text-center\" style='padding: 1rem'>";
    echo "    <a href=\"/devices\" class=\"btn btn-" . $occasion . " btn-lg\">";
    echo "        <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>";
    echo "        Zurück zur Geräteübersicht";
    echo "    </a>";
    echo "</div>";
}

?>

<!doctype html>
<html>
<head>
    <title>Geräte :: SMSM - Selfmade SmartHome</title>
    <link rel=stylesheet type=text/css href="/static/css/bootstrap.css">
    <link rel=stylesheet type=text/css href="/static/css/style.css">
    <link rel="icon" type="image/png" href="/static/img/favicon.png">
</head>
<body>
<!-- The upper navbar with logo and navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Left section with logo -->
        <div class="navbar-left">
            <a href="/" class="navbar-brand navbar-logo">
                <img src="/static/img/logo.svg"/>
                Selfmade SmartHome
            </a>
        </div>

        <!-- Right section with navigation -->
        <nav class="navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="/">Startseite</a></li>
                <li><a href="/devices">Geräte</a></li>
                <li><a href="#">Impressum</a></li>
                <li><a href="/help.php">Hilfe</a></li>
                <li><a href="/contact.php">Kontakt</a></li>
            </ul>
        </nav>
    </div>
</nav>

<div class="container main-container">
    <div class="page-header">
        <h1>Gerät bearbeiten</h1>
    </div>
    <div class="panel panel-default">
        <?php
        if ($_SERVER["REQUEST_METHOD"] === "GET") {
            if (isset($_GET["id"]) && is_numeric($_GET["id"])) {
                $device = $deviceRepository->findById(intval($_GET["id"]));

                if ($device != NULL) {
                    echo "<div class=\"panel-heading\">" . htmlentities($device->getName()) . " &#91;" . htmlentities($device->getMacAddress()) . " &#124; " . htmlentities($device->getIpAddress()) . "&#93;</div>";

                    echo "<div style=\"padding: 1rem\">";
                    echo "<form action=\"" . htmlentities($_SERVER["PHP_SELF"]) . "\" method=\"POST\">";
                    echo "    <input type=\"text\" name=\"id\" value=\"" . $device->getId() . "\" hidden>";
                    echo "    <div class='form-group'>";
                    echo "        <label for=\"input_name\">Name</label>";
                    echo "        <input type=\"text\" class=\"form-control\" id=\"input_name\" name=\"name\" value=\"" . $device->getName() . "\">";
                    echo "    </div>";
                    echo "    <div class='form-group'>";
                    echo "        <label for=\"input_location\">Standort</label>";
                    echo "        <div class=\"input-group\">";
                    echo "            <select class=\"form-control\" id=\"input_location\" name=\"location\">";
                    echo "                <option>Unbekannt</option>";

                    foreach ($locationRepository->findAll() as $location) {
                        $is_current_location = $device->getLocationId() != NULL && $device->getLocationId() == $location->getId();
                        echo "<option value=\"" . $location->getId() . "\" " . ($is_current_location ? "selected" : "") . ">" . htmlentities($location->getName()) . "</option>";
                    }

                    echo "            </select>";
                    echo "            <span class=\"input-group-btn\">";
                    echo "                <button type=\"button\" class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#addLocationModal\">...</button>";
                    echo "            </span>";
                    echo "        </div>";
                    echo "    </div>";
                    echo "<button type=\"submit\" class=\"btn btn-primary\">Speichern</button>";
                    echo "</form>";
                    echo "</div>";
                    generateButtonBackToDeviceOverviewForOccasion("primary");
                } else {
                    http_response_code(404);
                    echo "<h2 class='text-warning text-center'>Kein Gerät mit der gegebenen ID gefunden</h2>";
                    generateButtonBackToDeviceOverviewForOccasion("warning");
                }
            } else {
                http_response_code(400);
                echo "<h2 class='text-danger text-center'>Unbekannte oder invalide Geräte-ID</h2>";
                generateButtonBackToDeviceOverviewForOccasion("danger");
            }
        } else if ($_SERVER["REQUEST_METHOD"] === "POST") {
            if (isset($_POST["id"]) && is_numeric($_POST["id"])) {
                $device = $deviceRepository->findById(intval($_POST["id"]));

                if (isset($_POST["name"])) {
                    $device->setName($_POST["name"]);
                }

                if (isset($_POST["location"]) && is_numeric($_POST["location"])) {
                    $device->setLocationId(intval($_POST["location"]));
                }

                if ($deviceRepository->update($device)) {
                    http_response_code(200);
                    echo "<h2 class='text-success text-center'>Gerät erfolgreich aktualisiert</h2>";
                    generateButtonBackToDeviceOverviewForOccasion("success");
                } else {
                    http_response_code(500);
                    echo "<h2 class='text-danger text-center'>Whoops! Beim Speichern ist scheinbar etwas schiefgelaufen</h2>";
                    generateButtonBackToDeviceOverviewForOccasion("danger");
                }
            } else {
                http_response_code(400);
                echo "<h2 class='text-danger text-center'>Unbekannte oder invalide Geräte-ID</h2>";
                generateButtonBackToDeviceOverviewForOccasion("danger");
            }
        } else {
            http_response_code(405);
            echo "<h2 class='text-danger text-center'>Diese HTTP Methode ist hier nicht erlaubt</h2>";
            generateButtonBackToDeviceOverviewForOccasion("danger");
        }
        ?>
    </div>

    <div class="modal fade" id="addLocationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Neuen Standort hinzufügen</h4>
                </div>
                <div class="modal-body">
                    <form action="/locations/index.php" method="POST">
                        <div class="form-group">
                            <label for="location_name">Name</label>
                            <input type="text" class="form-control" id="location_name" name="name" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Speichern</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                </div>
            </div>
        </div>
    </div>

    <script src="/static/js/jquery.js"></script>
    <script src="/static/js/bootstrap.min.js"></script>
</body>
</html>
