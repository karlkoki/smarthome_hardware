<?php

namespace SMSM\Devices;

class DeviceFunctionRepository
{

    const DATABASE_FILE_PATH = __DIR__ . "/../../ccserver.db";

    /**
     * @return DeviceFunction[]
     */
    public function findAll()
    {
        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READONLY);

        $query = $db->query("SELECT * FROM gpio;");

        $functions = array();

        while ($result_row = $query->fetchArray(SQLITE3_ASSOC)) {
            array_push($functions, $this->mapRowToClass($result_row));
        }

        $db->close();

        return $functions;
    }

    /**
     * @param int $id
     * @return DeviceFunction|null
     */
    public function findById(int $id)
    {
        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READONLY);

        $query_stmt = $db->prepare("SELECT * FROM gpio WHERE id = :ID;");
        $query_stmt->bindValue("ID", $id);

        $query_result = $query_stmt->execute();

        $function = NULL;

        if ($result_row = $query_result->fetchArray(SQLITE3_ASSOC)) {
            $function = $this->mapRowToClass($result_row);
        }

        $db->close();

        return $function;
    }

    /**
     * @param int $hardwareId
     * @return DeviceFunction[]
     */
    public function findByHardwareId(int $hardwareId)
    {
        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READONLY);

        $query_stmt = $db->prepare("SELECT * FROM gpio WHERE hardware_id = :HARDWARE_ID;");
        $query_stmt->bindValue("HARDWARE_ID", $hardwareId);

        $query_result = $query_stmt->execute();

        $functions = array();

        while ($result_row = $query_result->fetchArray(SQLITE3_ASSOC)) {
            array_push($functions, $this->mapRowToClass($result_row));
        }

        $db->close();

        return $functions;
    }

    /**
     * @param DeviceFunction $deviceFunction
     * @param int            $deviceId
     * @return bool|int
     */
    public function insert(DeviceFunction $deviceFunction, int $deviceId)
    {
        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READWRITE);

        $insert_stmt = $db->prepare("INSERT INTO gpio (hardware_id, name, url) VALUES (:HARDWARE_ID, :NAME, :URL);");
        $insert_stmt->bindValue("HARDWARE_ID", $deviceId);
        $insert_stmt->bindValue("NAME", $deviceFunction->getName());
        $insert_stmt->bindValue("URL", $deviceFunction->getUrl());

        $result = ($insert_stmt->execute() !== false) ? $db->lastInsertRowID() : false;

        $db->close();

        return $result;
    }

    /**
     * @param int $hardwareId
     * @return bool
     */
    public function deleteByHardwareId(int $hardwareId)
    {
        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READWRITE);

        $delete_stmt = $db->prepare("DELETE FROM gpio WHERE hardware_id = :HARDWARE_ID;");
        $delete_stmt->bindValue("HARDWARE_ID", $hardwareId);

        $result = ($delete_stmt->execute() !== false);

        $db->close();

        return $result;
    }

    /**
     * @param array $db_row
     * @return DeviceFunction
     */
    private function mapRowToClass(array $db_row)
    {
        $func = new DeviceFunction($db_row["name"], $db_row["url"]);
        $func->setId($db_row["id"]);

        return $func;
    }

}