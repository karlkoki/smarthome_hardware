<!doctype html>
<html>
<head>
    <title>SMSM - Selfmade SmartHome</title>
    <link rel=stylesheet type=text/css href="/static/css/bootstrap.css">
    <link rel=stylesheet type=text/css href="/static/css/style.css">
    <link rel="icon" type="image/png" href="/static/img/favicon.png">
</head>
<body>
<!-- The upper navbar with logo and navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Left section with logo -->
        <div class="navbar-left">
            <a href="/" class="navbar-brand navbar-logo">
                <img src="/static/img/logo.svg"/>
                Selfmade SmartHome
            </a>
        </div>

        <!-- Right section with navigation -->
        <nav class="navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">Startseite</a></li>
                <li><a href="/devices">Geräte</a></li>
                <li><a href="#">Impressum</a></li>
                <li><a href="/help.php">Hilfe</a></li>
                <li><a href="/contact.php">Kontakt</a></li>
            </ul>
        </nav>
    </div>
</nav>

<div class="container main-container">
    <div class="page-header">
        <h1>Startseite</h1>
    </div>
</div>

<script src="/static/js/jquery.js"></script>
<script src="/static/js/bootstrap.min.js"></script>
</body>
</html>
