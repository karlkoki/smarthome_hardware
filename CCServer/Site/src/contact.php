<!doctype html>
<html>
<head>
    <title>Kontakt :: SMSM - Selfmade SmartHome</title>
    <link rel=stylesheet type=text/css href="/static/css/bootstrap.css">
    <link rel=stylesheet type=text/css href="/static/css/style.css">
    <link rel="icon" type="image/png" href="/static/img/favicon.png">
</head>
<body>
<!-- The upper navbar with logo and navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Left section with logo -->
        <div class="navbar-left">
            <a href="/" class="navbar-brand navbar-logo">
                <img src="/static/img/logo.svg"/>
                Selfmade SmartHome
            </a>
        </div>

        <!-- Right section with navigation -->
        <nav class="navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="#">Startseite</a></li>
                <li><a href="/devices">Geräte</a></li>
                <li><a href="#">Impressum</a></li>
                <li><a href="/help.php">Hilfe</a></li>
                <li><a href="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">Kontakt</a></li>
            </ul>
        </nav>
    </div>
</nav>

<div class="container main-container">
    <div class="page-header">
        <h1>Kontakt</h1>
    </div>
    <p>Firma: Gruppe 3 Smarthome Hardware</p>
    <p>Adresse: Grenzstraße 3, Gebäude 12, Raum 2.61, 24148 Kiel</p>
    <p>Tel.: 0800 123456</p>
    <p>E-Mail: <a href="mailto:helpme@selfmadesmarthome.com">helpme@selfmadesmarthome.com</a></p>
</div>

<script src="/static/js/jquery.js"></script>
<script src="/static/js/bootstrap.min.js"></script>
</body>
</html>
