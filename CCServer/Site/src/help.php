<?php

require_once __DIR__ . "/../vendor/autoload.php";

?>

<!doctype html>
<html>
<head>
    <title>Hilfe :: SMSM - Selfmade SmartHome</title>
    <link rel=stylesheet type=text/css href="/static/css/bootstrap.css">
    <link rel=stylesheet type=text/css href="/static/css/style.css">
    <link rel="icon" type="image/png" href="/static/img/favicon.png">
</head>
<body>
<!-- The upper navbar with logo and navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Left section with logo -->
        <div class="navbar-left">
            <a href="/" class="navbar-brand navbar-logo">
                <img src="/static/img/logo.svg"/>
                Selfmade SmartHome
            </a>
        </div>

        <!-- Right section with navigation -->
        <nav class="navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="#">Startseite</a></li>
                <li><a href="/devices">Geräte</a></li>
                <li><a href="#">Impressum</a></li>
                <li><a href="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">Hilfe</a></li>
                <li><a href="/contact.php">Kontakt</a></li>
            </ul>
        </nav>
    </div>
</nav>

<div class="container main-container">
    <div class="page-header">
        <h1>Hilfe</h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">README.md</div>

        <div style="padding: 1rem;">
            <?php
            if (file_exists(__DIR__ . "/../../../README.md")) {
                // Hier liegt die Datei in der Regel während der Entwicklung
                echo Parsedown::instance()
                    ->setMarkupEscaped(true)
                    ->text(file_get_contents(__DIR__ . "/../../../README.md"));
            } else if (file_exists(__DIR__ . "/help.md")) {
                // Hier liegt die Datei wenn die Anwendung auf dem Gerät installiert wurde
                echo Parsedown::instance()
                    ->setMarkupEscaped(true)
                    ->text(file_get_contents(__DIR__ . "/help.md"));
            } else {
                echo "<h2 class='text-danger text-center'>Konnte README.md nicht finden</h2>";
            }
            ?>
        </div>
    </div>

    <script src="/static/js/jquery.js"></script>
    <script src="/static/js/bootstrap.min.js"></script>
</body>
</html>
