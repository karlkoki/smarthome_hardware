<?php

use SMSM\Locations\Location;
use SMSM\Locations\LocationRepository;

require_once __DIR__ . "/../../vendor/autoload.php";

$locationRepository = new LocationRepository();

/**
 * Wenn der Nutzer von der "Gerät bearbeiten" Seite kommt, leite ihn wieder dorthin zurück.
 */
function performRedirectIfNecessary()
{
    if (isset($_SERVER["HTTP_REFERER"]) && strpos($_SERVER["HTTP_REFERER"], "/devices/edit.php") !== false) {
        http_response_code(302);
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
}

switch ($_SERVER["REQUEST_METHOD"]) {
    case "GET":
        http_response_code(200);
        header("Content-Type: application/json");
        echo json_encode($locationRepository->findAll());
        break;
    case "POST":
        $location = new Location(NULL, $_POST["name"]);
        if ($location->getName() !== NULL && $locationRepository->findByName($location->getName()) === NULL) {
            if ($result = $locationRepository->insert($location)) {
                // Location erfolgreich angelegt
                http_response_code(200);
                performRedirectIfNecessary();
                echo json_encode(array("id" => $result));
            } else {
                // Interner Fehler beim Speichern der Daten
                http_response_code(500);
                performRedirectIfNecessary();
            }
        } else {
            // Es wurde entweder kein Name mitgegeben oder es existiert bereits
            // eine Location mit diesem Namen
            http_response_code(400);
            performRedirectIfNecessary();
        }
        break;
    default:
        http_response_code(405);
}
