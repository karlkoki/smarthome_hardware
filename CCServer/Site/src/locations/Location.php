<?php

namespace SMSM\Locations;

class Location implements \JsonSerializable
{

    private $id;

    private $name;

    /**
     * @param int|null $id
     * @param string   $name
     */
    public function __construct($id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function jsonSerialize()
    {
        return array(
            "id" => $this->getId(),
            "name" => $this->getName()
        );
    }

}