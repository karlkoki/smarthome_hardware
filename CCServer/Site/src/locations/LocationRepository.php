<?php

namespace SMSM\Locations;

class LocationRepository
{

    const DATABASE_FILE_PATH = __DIR__ . "/../../ccserver.db";

    /**
     * @return Location[]
     */
    public function findAll()
    {
        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READONLY);

        $query = $db->query("SELECT * FROM location;");

        $locations = array();

        while ($result_row = $query->fetchArray(SQLITE3_ASSOC)) {
            array_push($locations, $this->mapRowToClass($result_row));
        }

        $db->close();

        return $locations;
    }

    /**
     * @param int $id
     * @return Location|null
     */
    public function findById(int $id)
    {
        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READONLY);

        $query_stmt = $db->prepare("SELECT * FROM location WHERE id = :ID;");
        $query_stmt->bindValue("ID", $id);

        $location = NULL;

        if ($result_row = $query_stmt->execute()->fetchArray(SQLITE3_ASSOC)) {
            $location = $this->mapRowToClass($result_row);
        }

        $db->close();

        return $location;
    }

    /**
     * @param string $name
     * @return null|Location
     */
    public function findByName(string $name)
    {
        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READONLY);

        $query_stmt = $db->prepare("SELECT * FROM location WHERE name = :NAME;");
        $query_stmt->bindValue("NAME", $name);

        $location = NULL;

        if ($result_row = $query_stmt->execute()->fetchArray(SQLITE3_ASSOC)) {
            $location = $this->mapRowToClass($result_row);
        }

        $db->close();

        return $location;
    }

    /**
     * @param Location $location
     * @return bool|int
     */
    public function insert(Location $location)
    {
        $db = new \SQLite3(self::DATABASE_FILE_PATH, SQLITE3_OPEN_READWRITE);

        $insert_stmt = $db->prepare("INSERT INTO location (name) VALUES (:NAME);");
        $insert_stmt->bindValue("NAME", $location->getName());

        $result = ($insert_stmt->execute() !== false) ? $db->lastInsertRowID() : false;

        $db->close();

        return $result;
    }

    /**
     * @param array $db_row
     * @return Location
     */
    private function mapRowToClass(array $db_row)
    {
        return new Location($db_row["id"], $db_row["name"]);
    }

}