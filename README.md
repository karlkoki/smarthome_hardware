# SmartHome Projekt - README		V1.4 14.06.2018

#### 1. Konfigurations des Command & Control Servers
    1.1 Voraussetzungen
    1.2 Vorbereitung
    1.3 Raspberry PI im WLAN Netzwerk anmelden
    1.4 Installieren der Server-Software
#### 2. ESP8266
    2.1 Arduino IDE
    2.2 Installation des Treibers ESP8266
    2.3 Webserver/GPIO Steuerung
#### 3. Smarthome Geräte Manager Website
    3.1 Geräte hinzufügen
    3.2 Geräte bearbeiten
    3.3 Geräte entfernen
    3.4 Weitere Funktionen
#### 4. Steckdosendesign mit Solidworks
	4.1 Anzeigen der Dateien
	4.2 Bearbeiten der Dateien
	4.3 3D-Druck
#### 5. Entwicklungsumgebungen
#### 6. Shopping List

# 1. Konfiguration des Command & Control Servers

## 1.1 Voraussetzungen

* WLAN-Router (bspw. FritzBox), ggfs. bereits bestehendes WLAN-Netzwerk mit Internet-Zugang
* Einen WLAN-fähigen [Raspberry Pi](https://www.raspberrypi.org/products/) ([Zero](https://www.raspberrypi.org/products/raspberry-pi-zero/))
* Eine Micro SD Karte, wir empfehlen eine Karten-Klasse von 10 mit einer Kapazizät von `>= 4GB`
	* Siehe hier auch den [offiziellen Guide](https://www.raspberrypi.org/documentation/installation/sd-cards.md) zu SD-Karten für den Raspberry Pi
* [Raspbian Lite](https://www.raspberrypi.org/downloads/raspbian/) in der Version `March 2018` oder neuer
* Bildschirm und Tastatur zum temporären anschließen an den Raspberry Pi
	* Für den Raspberry Pi *Zero* sind außerdem ein [Micro-HDMI auf HDMI Adapter](https://www.reichelt.de/A-V-Adapter-HDMI-/AD-HDMI-MICRO/3/index.html?ACTION=3&GROUPID=5385&ARTICLE=106954) und ein [Micro-USB auf USB-Adapter](https://www.reichelt.de/USB-2-0-Kabel/USB2-FL-AMB-WS/3/index.html?ACTION=3&LA=446&ARTICLE=131386&GROUPID=8279&artnr=USB2+FL+AMB+WS) erforderlich
	* Nach der Einrichtung ist der Pi auch via SSH von einem anderen PC aus erreichbar

## 1.2 Vorbereitung

* Dem [offiziellen Guide](https://www.raspberrypi.org/documentation/installation/installing-images/) zur Installation von Betriebssystemen folgen
* Dem [offiziellen Guide](https://www.raspberrypi.org/documentation/remote-access/ssh/) zur Aktivierung des SSH-Servers folgen
* Falls noch nicht vorhanden: Einrichten eines WLAN-Netzwerks mithilfe des Routers
	* Für diese Anleitung verwenden wir folgende Daten:
		* SSID: `SmartHome WLAN`
		* Passwort: `sh1234wlan`
* Bildschirm und Tastatur mit dem Raspberry Pi verbinden
* SD-Karte in den Raspberry Pi einsetzen und diesen an den Strom anschließen
* Warten, bis der Bootvorgang des Raspberry's abgeschlossen ist
* Mit den Default-Zugangsdaten einloggen:
	* Login: `pi`
	* Passwort: `raspberry`
	* **Achtung**: Das Tastatur-Layout des Pi's ist Standardmäßig für englische Tastaturen konfiguriert

## 1.3 Raspberry Pi im WLAN Netzwerk anmelden

* Starten des Raspberry Konfigurationstools: `sudo raspi-config`

![Hauptmenü von raspi-config](.readme/raspi-config.png)

* `Network Options` auswählen, im Folgemenu dann `Wi-fi`

![raspi-config Netzwerkeinstellungen](.readme/raspi-config_network.png)

* Nun die in der [Vorbereitung](#1-2-vorbereitung) festgelegte SSID eingeben (Sonder- bzw. Leerzeichen müssen nicht escaped werden)

![raspi-config Netzwerkname](.readme/raspi-config_network_name.png)

* Im Folgebildschirm dann noch mit dem Passwort bestätigen und raspi-config verlassen
* Zur Kontrolle folgenden Befehl eingeben: `ifconfig | grep inet\ `
	* Neben der IPv4-Adresse des Loopback Interfaces `127.0.0.1` sollte nun eine weitere hier aufgelistet sein (typischerweise `192.168.178.xx`)
* Testen der Internet-Verbindung mittels `ping 1.1.1.1` oder `ping 8.8.8.8`

## 1.4 Installieren der Server-Software

* Herunterladen und Ausführen des [`server-setup.sh`-Skripts](https://stash.iue.fh-kiel.de/projects/AEM/repos/smarthome_hardware/browse/CCServer/server-setup.sh?at=refs%2Fheads%2Fmaster&raw)
	* Aus dem **FH-IoT-NAT** Netzwerk: `curl -s "https://gitlab.com/karlkoki/smarthome_hardware/raw/master/CCServer/server-setup.sh" | sudo ${0#-} /dev/stdin`
	* Von **überall sonst**: `curl -s "https://stash.iue.fh-kiel.de/projects/AEM/repos/smarthome_hardware/browse/CCServer/server-setup.sh?at=refs%2Fheads%2Fmaster&raw" | sudo ${0#-} /dev/stdin`
	* Das Skript installiert benötigte Software-Pakete und stellt sicher, dass der Server korrekt eingerichtet wird
	* Ggfs. diesen Schritt von einem anderen PC aus via SSH durchführen, da der Befehl dort einfach über Copy & Paste eingegeben werden kann. Aufgrund der langen URL ist dies sonst sehr müßig. Zukünftig könnte das mittels eines URL Shortener Services behoben werden.


# 2. ESP8266

## 2.1 Arduino IDE
* Zum Programmieren für den ESP8266 nutzen wir die Arduino IDE Version 1.8.5 [Download Installer](https://www.arduino.cc/en/Main/Donate/). 
* Diese muss noch für den ESP8266 eingerichtet werden. Hierfür wurde die folgende Anleitung verwendet:  
[Anleitung](https://learn.sparkfun.com/tutorials/esp8266-thing-hookup-guide/installing-the-esp8266-arduino-addon/).
* Als letzten Schritt stellen wir noch unter: Werkzeuge -> Reset Method -> auf `nodemcu`.

## 2.2 Installation des Treibers ESP8266 
* Nur unter Windows notwendig.
* Zur installation der Treiber gehen wir unter Windows in den Gerätemanager. 
* Der ESP8266 sollte mit einem Fragezeichen versehen sein. 
Rechtsklicht auf den ESP8266 dann "Treiber aktualisieren" wählen. 
* Als nächstes wählen wir "Auf dem Computer nach Treibersoftware suchen" und navigieren in /.../arduino-1.8.5/driver und bestätigen.

## 2.3 Webserver/GPIO Steuerung
* Um dies zu ermöglichen kopieren wir die Software aus unserem git MSP8266/wlanSteckdose/wlanSteckdose.ino in die Arduino IDE.
* Es müsssen nun lediglich die SSID und das Passwort für die Verbindung mit dem eigenen WLAN geändert werden. Dann kann man mit dem 
Pfeil in der oberen linken Ecke die Software auf den ESP8266 flashen.
* Der ESP8266 sollte sich nun automatisch mit dem ausgewählten WLAN verbinden und man kann die GPIOs nun mit :

* ip/d3/1
* ip/d3/0
* ip/d4/1
* ip/d4/0
* ip/d1/1
* ip/d1/0
* ip/d2/1
* ip/d2/0

schalten.


* Über den Seriellen Monitor (Lupensymbol oben rechts) in der Arduino IDE kann nun die IP des ESP8266 im WLAN angezeigt werden. 
* Im Browser: http://"IP-Adresse"/info: hier werden dem Nutzer alle Befehle ausgegeben, welche er über den Browser eingeben kann,
um seine Smarthome-Geräte zu steuern.


## 3.Smarthome Geräte Manager Website
* Die Seite ist innerhalb des zuvor eingerichteten Netzwerks unter der IP-Adresse des Raspberry Pi's erreichbar
* Einfach `http://{ip-adresse-des-pi}` in einem beliebigem Web-Browser aufrufen
* Unter dem Menüpunkt `Geräte` werden alle Geräte aufgelistet, beim ersten Start ist diese Übersicht natürlich leer

![Geräteübersicht beim ersten Start](.readme/website-geräteübersicht-leer.png)


## 3.1 Geräte hinzufügen

* Mit einem Klick auf *Nach Geräten im Netzwerk suchen* kann die automatische Erkennung unterstützter Geräte im Netzwerk gestartet werden
* Die Suche dauert 5 Sekunden, danach werden alle erkannten Geräte in der Übersicht angezeigt:

![Geräteübersich nach erfolgreicher automatischer Erkennung](.readme/website-geräteübersicht-nach-suche.png)

* Durch einen Klick auf den Zahnrad-Button unter *Optionen* wird eine Auflistung aller Funktionen des Geräts angezeigt, die durch einen Klick ausgelöst werden können:

![Funktionen des Geräts](.readme/website-geräteübersicht-funktionen.png)

## 3.2 Geräte bearbeiten

* Rechts neben dem Zahnrad-Button findet sich ein *Bearbeiten*-Button
* Nach Betätigung des Buttons gibt es die Möglichkeit den Namen und den Standort des Geräts zu bearbeiten:

![Gerät bearbeiten](.readme/website-gerät-bearbeiten.png)

* Hier ist es auch möglich, durch einen Klick auf `...` einen neuen Standort anzulegen:

![Einen neuen Standort anlegen](.readme/website-standort-anlegen.png)

## 3.3 Geräte entfernen

* Durch einen Klick auf das rote *Mülleimer*-Symbol kann ein Gerät entfernt werden
* **Achtung!** Ist ein Gerät einmal entfernt, wird es bei späteren Suchen nach Geräten im Netzwerk nicht mehr berücksichtigt!

## 3.4 Weitere Funktionen

* *Alle Funktionen aktivieren* / *Alle Funktionen deaktivieren*
  * Manche Geräte unterstützen das Aktivieren / Deaktivieren aller ihrer Funktionen auf einmal
  * Durch die beiden Buttons lassen sich diese "*All On*" bzw. "*All Off*" Funktionen aller verfügbarer Geräte auf einmal auslösen
* *Hilfe*
  * Zeigt den Inhalt dieser README an
* *Kontakt*
  * Zeigt die Kontaktdaten unseres Projekts an

## 4. Steckdosendesign mit Solidworks
Hinweise für den Umgang mit den im Ordner "Steckdosenleiste-Design" enthaltenen Dateien.

## 4.1 Anzeigen der Dateien
Es wird empfohlen den "eDrawingsViewer" zu benutzen um die in Solidworks erstellten `.sldprt`-Dateien oder auch die exportierten `.stl`-Dateien anzuzeigen.
Der nachfolgende Link führt zum Download des Programms. Dort gibt es außerdem Informationen darüber, welche Betriebssysteme unterstützt werden.
* [eDrawingsViewer](http://www.edrawingsviewer.com/ed/download.htm)

## 4.2 Bearbeiten der Dateien
Die `.sldprt`-Dateien können mit dem kommerziell erhältlichen Solidworks in der Version "**Solidworks 2016-2017" oder neuer** geöffnet und bearbeitet werden.
Wird versucht diese Dateien mit einer älteren Version zu öffnen, kommt es zu Kompatibilitätsproblemen.

## 4.3 3D-Druck
Für den 3D-Druck der Steckdosenteile werden die `.stl`-Dateien benötigt.
Es ist jedoch auch unter Berücksichtigung von 4.2 möglich selbst die `.sldprt`-Dateien in ein anderes Format zu exportieren.



## 5. Entwicklungsumgebungen

* [PHPStorm 2018.1.4](https://blog.jetbrains.com/webstorm/tag/webstorm-2018-1/)
* [Arduino IDE 1.8.5](https://www.arduino.cc/en/Main/Donate/)
* [Solidworks 2016-2017](http://www.solidworks.de/)

## 6. Shopping List

* [Raspberry Pi](https://www.raspberrypi.org/products/)
* [Raspbian Lite](https://www.raspberrypi.org/downloads/raspbian/)
* [ESP8266](https://www.amazon.de/AZDelivery-NodeMCU-ESP8266-ESP-12E-Development/dp/B06Y1LZLLY/ref=sr_1_3?ie=UTF8&qid=1528463728&sr=8-3&keywords=ESP8266/)
* [ESP8266 Cheaper Version](https://www.aliexpress.com/item/ESP8266-ESP-12-NodeMCU-Lua-WiFi-Internet-Things-Development-Board/32368848967.html?spm=2114.search0204.3.34.3a5fd3f0WZege3&ws_ab_test=searchweb0_0,searchweb201602_4_10320_10152_10321_10065_10151_10344_10068_5722815_10342_10547_10343_10322_10340_10341_5722915_10548_5722615_10193_10696_10194_10084_10083_10618_10304_10307_10820_10821_10302_5722715_10059_100031_10319_10103_10624_10623_10622_5722515_10621_10620,searchweb201603_55,ppcSwitch_3_ppcChannel&algo_expid=66852b1c-f551-4e50-bc7d-6f3d9893d07b-8&algo_pvid=66852b1c-f551-4e50-bc7d-6f3d9893d07b&priceBeautifyAB=0/)

